using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.CompareTag("Player"))
        {
           
            FindObjectOfType<GameManager>().OnCollectableReach();
           
            Destroy(gameObject);

            FindObjectOfType<GameManager>().UpdateObjectCount();
        }
    }

}
