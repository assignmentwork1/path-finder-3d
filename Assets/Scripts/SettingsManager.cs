using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SettingsManager : MonoBehaviour
{
    public AudioMixer audioMixer;

    bool isMuted = false;

    public GameObject audioOnIcon;
    public GameObject audioOffIcon;

    public void MuteAudio()
    {
        audioMixer.SetFloat("MasterVolume", -80f);

        isMuted = true;
        audioOffIcon.SetActive(true);
        audioOnIcon.SetActive(false);
    }

    public void UnmuteAudio()
    {
        audioMixer.SetFloat("MasterVolume", 0f);

        isMuted = false;
        audioOnIcon.SetActive(true);
        audioOffIcon.SetActive(false);
    }

    public void ToggleAudio()
    {
        if(isMuted)
        {
            UnmuteAudio();

        }
        else
        {
            MuteAudio();
        }
    }


}
