using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using static UnityEngine.GraphicsBuffer;

public class GameManager : MonoBehaviour
{
    public AudioSource gameAudioSource;
    public AudioClip collectClip;

    public PlayerController controller;

    public GameObject collectablePrefab;

    int maxLength = 10;

    private List<GameObject> collectableQueue= new List<GameObject>();


    public int points=0;

    public Vector3 defaultPosition;

    public TextMeshProUGUI objectCount;
    public TextMeshProUGUI ScoreText;
    public TextMeshProUGUI distanceText;

    void Start()
    {
        defaultPosition=controller.transform.position;
    }

   
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity) && collectableQueue.Count < maxLength) 
            {
                if (!hit.transform.gameObject.CompareTag("Unaccessable"))
                {
                    GameObject NewCollectable = Instantiate(collectablePrefab, hit.point, Quaternion.identity);
                    collectableQueue.Add(NewCollectable);

                   
                    UpdateObjectCount();

                    UpdatePlayerTargetPosition();


                   
                }
            }
        }

        if(collectableQueue.Count> 0)
        {
            float distance = Vector3.Distance(controller.transform.position, collectableQueue[0].transform.position);


            distanceText.text = "Distance: " + distance.ToString("F0");
        }
        else
        {
            distanceText.text = "Distance:0 ";
        }
       
    }

    public void UpdatePlayerTargetPosition()
    {   

        collectableQueue[0].GetComponent<Collider>().enabled = true;

        //Vector3 targetPosition = collectableQueue[0].transform.position;

        controller.MovePlayer(collectableQueue[0].transform.position);

        //float distance = Vector3.Distance(controller.transform.position, targetPosition);

        //Debug.Log("Distance to target: " + distance);
    }

    public void OnCollectableReach()
    {
        gameAudioSource.clip = collectClip;
        gameAudioSource.Play();

        points+=10;
        ScoreText.text = "Score: " + points;
        collectableQueue.RemoveAt(0);

        

        if (collectableQueue.Count > 0)
        {
            UpdatePlayerTargetPosition();
        }
        else
        {
            controller.MovePlayer(defaultPosition);
        }
      

    }

    public void UpdateObjectCount()
    {
        objectCount.text = "Count: " + collectableQueue.Count;
    }
}
